$(function(){
    //scroll
    var scroll = $(this).scrollTop();
    $(window).scroll(function(){
        if($(this).scrollTop() >= 500){
            $('.main-nav-bar .navbar').addClass('fixed-top');
        }else{
            $('.main-nav-bar .navbar').removeClass('fixed-top');
        }
    });

    $('.our-portfolio ul li').on('click',function(){
        $(this).addClass('active').siblings().removeClass('active');
            //console.log($(this).data('class'));
        if($(this).data('class') === 'all'){
            $('.shuffile_images .box').css('opacity',1);
            
        }else{
            $('.shuffile_images .box').css('opacity','.3')

            $($(this).data('class')).parent().css('opacity',1);
        }
    });

    $('.our-portfolio .shuffile_images img').on('click',function(){
        //giv her src
        var attr_src = $(this).attr('src');
        $('.gallery-img img').attr('src',attr_src);
        $('.gallery-img').fadeIn();

        $('.gallery-img').on('click',function(e){
            e.stopPropagation();
            $(this).fadeOut(500);
        });
        $('.gallery-img img,.icon').on('click',function(e){
            e.stopPropagation();
        });
        $('.gallery-img .exit').on('click',function(){
            $('.gallery-img').fadeOut(500);
        });

        $('.gallery-img .arrows').on('click',function(){
            $('.gallery-img img').toggleClass('full-screen');
        });
    });
});

